# Spring Boot Java 17

This is my playground for Spring Boot Java 17. - And for future devs to read 
Hope this goes well. 

# IDE and Hot Reload
I'm not fond of Eclipse (only dbeaver) nor Jetbrain IntelliJ, because it's heavy and harder to tweak than VS Code (this is opionated, so please don't judge me). We can install a lot of extensions for debugging and hot reload in VS Code (and vs code support built in hot reload).
In short we need :
1. [Java extension pack](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack) - Debug and Running
2. [Gradle VS Code](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-gradle) - Running the Project, sorry mvn/maven, I seen that gradle is faster about 50% of the time, so... see ya
3. [Spring Extension Pack](https://marketplace.visualstudio.com/items?itemName=vmware.vscode-boot-dev-pack) - For check the endpoint and everything else related to Springs (generat new project, beans, etc)

Optional :
1. [Red Hat Dependency Analytics](https://marketplace.visualstudio.com/items?itemName=redhat.fabric8-analytics) - For Security

After all tools are installed, we need to enable the config on VS Code and gradle to enable hot reload (like in dotnet, well.. I LOVE DOTNET.. whatever), as step below:

First add the config to the gradle :
```gradle
dependencies {
	developmentOnly("org.springframework.boot:spring-boot-devtools")
}
```
remember, since 2019 gradle doesn't support `compile` command, so we can swap it with `runtimeOnly`, [Hot Reload VS Code](https://stackoverflow.com/questions/46144346/hot-swapping-in-spring-boot-in-visual-studio-code), [Compile Deprecated](https://stackoverflow.com/a/66910991/4906348), and [Gradle Java Plugin Library Graph](https://docs.gradle.org/7.0/userguide/java_library_plugin.html#sec:java_library_configurations_graph)

Then after that, check on vs code config, and enable the hot code replace [hot replace config](https://stackoverflow.com/a/64650176/4906348)

<img src='https://i.stack.imgur.com/Tjfpm.png' style='witdh:100%'>

Then we can use `F5` on the VS Code to enable hot reload (sometimes doesn't work, but sometimes does after the config enabled). I haven't able to use `./gradlew bootRun` to enable hotreload on CLI and use attach like on `dotnet watch`. Well at least it seems fast on VS Code using Spring than `dotnet run` (I'm being honest here, it's faster using gradle+spring boot when debugging).

Workaround if it's not working, is making `gradle` run as `watcher`, with command 

```shell
./gradlew -t build
#...output ommited
# if it's right, then it will show
BUILD SUCCESSFUL in 682ms
7 actionable tasks: 7 up-to-date

Waiting for changes to input files of tasks... (ctrl-d to exit)
<=============> 100% EXECUTING [3m 39s]
```

[About -t Flag on Gradle](https://stackoverflow.com/a/31459777/4906348) | [Build Gradle](https://stackoverflow.com/questions/67194033/spring-boot-devtools-automatic-restart-not-working/67278603#67278603)

And you are set!

# Code Structure and Pattern
## Controller 
We can make controller using `@RestController` or `@Controller` annotation for the controller class, then use `@GetMapping("/path/to/url")` or `@PostMapping("/path/to/url")`, then for the body request use `@RequestBody` annotation, example class :

controller 
```java
package id.salt.restservice.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import id.salt.restservice.Models.User;
import id.salt.restservice.Models.Responses.ApiResponse;

@RestController
public class UsersController {
    private List<User> data = new ArrayList<User>();
    
    // @PostConstruct
    public UsersController()
    {
        User temp;
        for (int i = 0; i < 10; i++) {
            temp = new User((long) (i+1), (Character.toChars(i).toString()));    
            data.add(temp);
        }
    }

    @GetMapping("/users")
    public List<User> getListUser() {
        return data;
    }

    @PostMapping("/users")
    public ApiResponse insertUser(@RequestBody User user)
    {
        user.id = this.data.size()+1;
        this.data.add(user);
        return new ApiResponse(200, "Success Insert Data", null);
    }
}
```

model request body
```java 
package id.salt.restservice.Models;

public class User {
    public long id; 
    public String username;

    public User()
    {
        
    }

    public User(long id, String username) {
        this.id = id; this.username = username;
    }
}
```

Read more [Controller Article Baeldung](https://www.baeldung.com/spring-controllers), [Introduction to Springs](https://spring.io/guides/gs/rest-service/#_create_a_resource_controller)

## Validation

For validation, we need the hibernation validation [source 1](https://www.codejava.net/frameworks/spring-boot/rest-api-request-validation-examples), [source 2](https://www.baeldung.com/spring-boot-bean-validation), so in short, in gradle add 2 dependency we need [jakarta bean validation](https://mvnrepository.com/artifact/jakarta.validation/jakarta.validation-api/3.0.2) and [spring start validation](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-validation/3.1.4) :
```gradle
dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-web'
	// https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-validation
	implementation group: 'org.springframework.boot', name: 'spring-boot-starter-validation', version: '3.1.4'
	// https://mvnrepository.com/artifact/jakarta.validation/jakarta.validation-api
	implementation group: 'jakarta.validation', name: 'jakarta.validation-api', version: '3.0.2'
	testImplementation('org.springframework.boot:spring-boot-starter-test')
	runtimeOnly("org.springframework.boot:spring-boot-devtools")
}
```

then we can put decorator/annotation on the class we want to have validation, eg :

```java 
package id.salt.restservice.Models.Requests;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class ContactUsRequest {
    @NotNull(message = "hai")
    @Size(min = 3, max = 20, message = "What")
    public String name;
    @NotBlank
    public String email;
    @NotBlank
    public String message;
}

```

then put the `@Valid` annotation and the `BindingRequest` in the method, so we can have make it check if it's error or not, eg :

```java
// This is only some part of the code, for more see the Controllers/ContactUsController.java
@PostMapping("/contact-us")
public ApiResponse ContactUsAction(@Valid ContactUsRequest request, BindingResult bindingResult)
{
    if (bindingResult.hasErrors())
    {
        return new ApiResponse(401, "Error validation", bindingResult.getAllErrors());
    }

    data.add(request);
    return new ApiResponse("Success Saving Contact US");
}
```
<div style="background:yellow; padding: 6px; border-radius: 8px">
<b>make sure that you already add the <code>spring-boot-strater-test</code>, if not then it won't even validate anything and always return false on <code>hasErrors</code> method**</b>
</div>

## Models

The Models depends on spring-data annotation project [docs](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation), it extending a interface, that will create an ORM, for short tuts, we can read [Access data with JPA](https://spring.io/guides/gs/accessing-data-jpa/), just don't use in memory database/RAM. In short, declare a class, and use annotation like in dotnet, for relationship, we can use like laravel/dotnet data annotation concept, and it will work directly. 

```java
package id.salt.restservice.Models;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column(name = "pricing")
    private long pricing;

    // @Column(name = "user_id")
    // private long userId;
    
    // public long getUserId() {
    //     return userId;
    // }

    @Column(name = "name")
    private String name;

    @Column(name = "notes")
    private String notes;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonManagedReference // @see https://stackoverflow.com/a/18288939/4906348 prevent infinite loop
    private User user;

    public User getUser() {
        return user;
    }

    public Product() {}

    public Product(long id, String name, long pricing)
    {
        this.id = id;
        this.name = name;
        this.pricing = pricing;
    }

    public void setId(long id)
    {
        this.id = id;
    }
    
    public long getId() {
		return id;
	}

    public String getName() 
    {
        return name;
    }

    public long getPricing()
    {
        return this.pricing;
    }

    public String getNotes()
    {
        return this.notes;
    }
}

```

## Security

The security depends on some package like `jjwt-api` and `spring-security` project, in short we need include those dependencies in `build.gradle`

```gradle
dependencies {
	// This is for JWT, this is bigger than auth0 JWT.io implementtaion
	// https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt-api
	implementation group: 'io.jsonwebtoken', name: 'jjwt-api', version: '0.12.3'
	// https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt-impl
	runtimeOnly group: 'io.jsonwebtoken', name: 'jjwt-impl', version: '0.12.3'
	// https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt-jackson
	runtimeOnly group: 'io.jsonwebtoken', name: 'jjwt-jackson', version: '0.12.3'

	runtimeOnly 'org.postgresql:postgresql'
	testImplementation 'org.springframework.boot:spring-boot-starter-test'
	// https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-validation
	implementation group: 'org.springframework.boot', name: 'spring-boot-starter-validation', version: '3.1.5'
	// https://mvnrepository.com/artifact/jakarta.validation/jakarta.validation-api
	implementation group: 'jakarta.validation', name: 'jakarta.validation-api', version: '3.0.2'
}
```