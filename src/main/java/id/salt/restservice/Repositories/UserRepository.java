package id.salt.restservice.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

import id.salt.restservice.Models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    
    List<User> findByFullNameLikeOrEmailLike(String name, String email);
}
