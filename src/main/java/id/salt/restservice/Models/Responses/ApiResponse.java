package id.salt.restservice.Models.Responses;

import java.util.HashMap;

public record ApiResponse(int statusCode, String message, Object data) {
    public ApiResponse()
    {
        this(200, "SUCCESS", null);
    }

    public ApiResponse(String message)
    {
        this(200, message, null);
    }

    public ApiResponse(int statusCode, String message)
    {
        this(statusCode, message, null);
    }

    public ApiResponse(Object data)
    {
        this(200, "Success fetch data", data);
    }

    public ApiResponse(String message, Object data)
    {
        this(200, message, data);
    }

    public ApiResponse(String message, HashMap<String, Object> data)
    {
        this(200, message, data);
    }
}
