package id.salt.restservice.Models;

// @see https://www.baeldung.com/java-record-keyword
public record Greeting(long number, String name) {}

