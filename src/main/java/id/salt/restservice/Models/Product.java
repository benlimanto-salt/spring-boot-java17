package id.salt.restservice.Models;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column(name = "pricing")
    private long pricing;

    // @Column(name = "user_id")
    // private long userId;
    
    // public long getUserId() {
    //     return userId;
    // }

    @Column(name = "name")
    private String name;

    @Column(name = "notes")
    private String notes;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonManagedReference // @see https://stackoverflow.com/a/18288939/4906348 prevent infinite loop
    private User user;

    public User getUser() {
        return user;
    }

    public Product() {}

    public Product(long id, String name, long pricing)
    {
        this.id = id;
        this.name = name;
        this.pricing = pricing;
    }

    public void setId(long id)
    {
        this.id = id;
    }
    
    public long getId() {
		return id;
	}

    public String getName() 
    {
        return name;
    }

    public long getPricing()
    {
        return this.pricing;
    }

    public String getNotes()
    {
        return this.notes;
    }
}
