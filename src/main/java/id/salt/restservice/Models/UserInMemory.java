package id.salt.restservice.Models;

public class UserInMemory {
    public long id; 
    public String username;

    public UserInMemory()
    {
        
    }

    public UserInMemory(long id, String username) {
        this.id = id; this.username = username;
    }
}
