package id.salt.restservice.Models;

import java.util.Collection;
import java.util.Set;
import java.util.ArrayList;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;


@Entity
@Table(name = "user", schema = "public")
public class User implements UserDetails {
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String username;

    private String password;

    @Column(name = "full_name")
    private String fullName;
    private String email;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    @JsonBackReference // @see https://stackoverflow.com/a/18288939/4906348, prevent inifinite loop
    private Set<Product> products;

    public User() {}

    public User(String username, String email, String password, String fullName)
    {
        this.username = username;
        this.email = email;
        this.password = password;
        this.fullName = fullName;
    }
    
    public Set<Product> getProducts() {
        return products;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    @JsonIgnore
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getFullName() {
        return fullName;
    }
    
    public String getEmail() {
        return email;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        var auth = new SimpleGrantedAuthority("ROLE_USER");
        Collection<GrantedAuthority> authCollection = new ArrayList<GrantedAuthority>();
        authCollection.add(auth);
        return authCollection;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    
}
