package id.salt.restservice.Models.Requests;

// For request, it's sad.. we really need to have.. getter setter rather than POCO/JOCO
// PLAIN OBJECT WITHOUT GETTER SETTER... JAVA I HATE YOU THIS TIME!
public class GuestBookRequest extends ContactUsRequest {
    public void setName(String name)
    {
        this.name = name;
    }
    
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getName()
    {
        return this.name;
    }

    public String getEmail()
    {
        return this.email;
    }
}
