package id.salt.restservice.Models.Requests;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class ContactUsRequest {
    @NotNull(message = "hai")
    @Size(min = 3, max = 20, message = "What")
    public String name;
    @NotBlank
    @Email
    public String email;
    @NotBlank
    public String message;
    
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
