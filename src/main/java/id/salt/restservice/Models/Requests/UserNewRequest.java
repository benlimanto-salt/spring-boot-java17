package id.salt.restservice.Models.Requests;

import com.fasterxml.jackson.annotation.JsonIgnore;

import id.salt.restservice.Models.User;
import jakarta.validation.constraints.NotNull;

public class UserNewRequest {
    @NotNull
    public String username;
    @NotNull
    public String password;
    public String fullName;
    public String email;

    @JsonIgnore
    public User ToUser()
    {
        return new User(username,email, password, fullName);
    }
}
