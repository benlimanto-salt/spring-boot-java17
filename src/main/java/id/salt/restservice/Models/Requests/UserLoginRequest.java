package id.salt.restservice.Models.Requests;


import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class UserLoginRequest {
    @NotNull
    @Size(min = 3)
    public String username;

    @NotNull
    @Size(min = 3)
    public String password;
}
