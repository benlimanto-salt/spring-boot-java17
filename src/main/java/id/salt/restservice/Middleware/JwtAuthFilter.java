package id.salt.restservice.Middleware;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.salt.restservice.Models.Responses.ApiResponse;
import id.salt.restservice.Services.JwtService;
import id.salt.restservice.Services.UserService;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

// This bean quite strange, because it's a filter component
// it should be @middleware, but Spring doesn't has it.
// This code based on, or rather copy pasted from 
// @see https://www.geeksforgeeks.org/spring-boot-3-0-jwt-authentication-with-spring-security-using-mysql-database/
// I don't use MySQL, but PostgreSQL
@Component
public class JwtAuthFilter extends OncePerRequestFilter {
    
    @Autowired
    JwtService jwtService;

    @Autowired
    UserService userService;

    /**
     * By default, it's all blocked, when we put this filter
     * We can set it to be having the user, by default deny, then we can give allow 
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        
        String authHeader = request.getHeader("Authorization"); 
        String token = null; 
        String username = null; 
        if (authHeader != null && authHeader.startsWith("Bearer ")) { 
            token = authHeader.substring(7); 
            try {
                username = jwtService.extractUsername(token);    
            } catch (Exception e) {
                response.resetBuffer();
                // @see https://stackoverflow.com/a/63057859/4906348
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                var resBody = new ApiResponse(403, "User : "+username+", is not allowed to access, because "+e.getMessage());
                response.setContentType("application/json; charset=utf-8");
                response.getWriter().write(new ObjectMapper().writeValueAsString(resBody));
                response.getWriter().flush(); // marks response as committed -- if we don't do this the request will go through normally!
                return; // Stop the function process other chain
            }
        } 
        
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) { 
            UserDetails userDetails = userService.loadUserByUsername(username); 
            if (jwtService.validateToken(token, userDetails)) { 
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities()); 
                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request)); 
                SecurityContextHolder.getContext().setAuthentication(authToken); 
            } 
        }

        filterChain.doFilter(request, response); 
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        ArrayList<String> exclude = new ArrayList<String>();
        exclude.add("/api/users/login");
        return exclude.contains(request.getRequestURI());
    }
    
}
