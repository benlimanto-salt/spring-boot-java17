package id.salt.restservice.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.List;

import id.salt.restservice.Models.User;

import id.salt.restservice.Repositories.UserRepository;

@Service
public class UserService implements UserDetailsService {
    
    @Autowired
    private UserRepository repo;

    public List<User> getList(Integer page, Integer limit)
    {
        if (page == null) page = 0;
        else page--;
        if (limit == null) limit = 10;

        Pageable pagination = PageRequest.of(page, limit);
        return repo.findAll(pagination).toList();
    }

    public List<User> tryLike(String name, String email)
    {
        if (name == null) name = "";
        if (email == null) email = ""; 
        return repo.findByFullNameLikeOrEmailLike("%"+name+"%", "%"+email+"%");
    }

    public User get(Integer id)
    {
        return repo.findById(id).orElse(null);
    }

    public User get(String username)
    {
        var u = new User();
        u.setUsername(username);

        return repo.findOne(Example.of(u)).orElse(null);
    }

    public Integer insert(User u)
    {
        return repo.save(u).getId();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var u = new User();
        u.setUsername(username);

        return repo
            .findOne(Example.of(u))
            .orElseThrow(() -> new UsernameNotFoundException("Username : "+username+" isn't found"));
        
    }
}
