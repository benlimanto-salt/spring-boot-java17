package id.salt.restservice.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import id.salt.restservice.Models.Product;
import id.salt.restservice.Repositories.ProductRepository;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository repository;

    public Product get(long id)
    {
        return repository.findById(id).orElse(null);
    }

    public List<Product> getList(Integer page, Integer limit)
    {
        if (page == null) page = 0;
        else page--;
        if (limit == null) limit = 10;

        // @see https://www.baeldung.com/spring-data-jpa-pagination-sorting
        Pageable pagination = PageRequest.of(page, limit);
        
        return repository.findAll(pagination).toList();
    }

    public long create(Product product)
    {
        return repository.saveAndFlush(product).getId();
    }

    public void update(Product product)
    {
        repository.save(product);
    }

    public void delete(Product product)
    {
        delete(product.getId());
    }

    public void delete(long id)
    {
        repository.deleteById(id);
    }
}
