package id.salt.restservice.Services;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import org.springframework.web.multipart.MultipartFile;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;

import id.salt.restservice.Config.StorageProperties;

@Service
public class StorageService {

	private final Path rootLocation;

	@Autowired
	public StorageService(StorageProperties properties) throws Exception {
        
        if(properties.getLocation().trim().length() == 0){
            throw new Exception("File upload location can not be Empty."); 
        }

		this.rootLocation = Paths.get(properties.getLocation());
	}

	public void store(InputStream file) throws IOException {
		String randomNewName = NanoIdUtils.randomNanoId();
		Path destinationFile = this.rootLocation.resolve(
		Paths.get(randomNewName))
			.normalize().toAbsolutePath();

		Files.copy(file, destinationFile, StandardCopyOption.REPLACE_EXISTING);

		String type = URLConnection.guessContentTypeFromStream(new BufferedInputStream(file));

		Files.move(destinationFile,  this.rootLocation.resolve(
			Paths.get(randomNewName + "." + type))
			.normalize().toAbsolutePath(), 
			StandardCopyOption.ATOMIC_MOVE);
		
	}

	public void store(MultipartFile file) throws Exception {
		try {
			if (file.isEmpty()) {
				throw new Exception("Failed to store empty file.");
			}
			Path destinationFile = this.rootLocation.resolve(
					Paths.get(file.getOriginalFilename()))
					.normalize().toAbsolutePath();
			if (!destinationFile.getParent().equals(this.rootLocation.toAbsolutePath())) {
				// This is a security check
				throw new Exception(
						"Cannot store file outside current directory.");
			}
			try (InputStream inputStream = file.getInputStream()) {
				Files.copy(inputStream, destinationFile,
					StandardCopyOption.REPLACE_EXISTING);
			}
		}
		catch (IOException e) {
			throw new Exception("Failed to store file.", e);
		}
	}

	public Stream<Path> loadAll() throws Exception {
		try {
			return Files.walk(this.rootLocation, 1)
				.filter(path -> !path.equals(this.rootLocation))
				.map(this.rootLocation::relativize);
		}
		catch (IOException e) {
			throw new Exception("Failed to read stored files", e);
		}

	}

	public Path load(String filename) {
		return rootLocation.resolve(filename);
	}

	public Resource loadAsResource(String filename) throws FileNotFoundException {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			}
			else {
				throw new FileNotFoundException(
						"Could not read file: " + filename);

			}
		}
		catch (MalformedURLException e) {
			throw new FileNotFoundException("Could not read file: " + filename);
		}
	}

	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	public void init() throws Exception {
		try {
			Files.createDirectories(rootLocation);
		}
		catch (IOException e) {
			throw new Exception("Could not initialize storage");
		}
	}
}
