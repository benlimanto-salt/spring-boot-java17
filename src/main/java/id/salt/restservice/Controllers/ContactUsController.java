package id.salt.restservice.Controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import id.salt.restservice.Models.Requests.ContactUsRequest;
import id.salt.restservice.Models.Responses.ApiResponse;
import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.RequestMapping;


@RestController
@RequestMapping(value="/api")
public class ContactUsController {
    protected List<ContactUsRequest> data = new ArrayList<ContactUsRequest>();

    public ContactUsController()
    {

    }

    // @see for response entity https://stackoverflow.com/a/48105864/4906348
    @PostMapping("/contact-us")
    public ResponseEntity<?> ContactUsAction(@RequestBody @Valid ContactUsRequest request, BindingResult bindingResult)
    {
        if (bindingResult.hasErrors())
        {
            return new ResponseEntity<>(new ApiResponse(400, "Error validation", bindingResult.getAllErrors()), HttpStatus.BAD_REQUEST);
        }

        data.add(request);
        // Other ways to write body
        return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(new ApiResponse("Success Saving Contact US"));
    }
}
