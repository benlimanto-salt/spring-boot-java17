package id.salt.restservice.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.salt.restservice.Models.UserInMemory;
import id.salt.restservice.Models.Responses.ApiResponse;


@RestController
@RequestMapping(path = "/in-memory")
public class UsersInMemoryController {
    private List<UserInMemory> data = new ArrayList<UserInMemory>();
    
    // @PostConstruct
    public UsersInMemoryController()
    {
        UserInMemory temp;
        for (int i = 0; i < 10; i++) {
            temp = new UserInMemory((long) (i+1), (Character.toChars(i).toString()));    
            data.add(temp);
        }
    }

    @GetMapping("/users")
    public List<UserInMemory> getListUser() {
        return data;
    }

    @PostMapping("/users")
    public ApiResponse insertUser(@RequestBody UserInMemory user)
    {
        user.id = this.data.size()+1;
        this.data.add(user);
        return new ApiResponse(200, "Success Insert Data", null);
    }
}
