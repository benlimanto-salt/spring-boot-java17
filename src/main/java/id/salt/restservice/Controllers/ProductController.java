package id.salt.restservice.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import id.salt.restservice.Models.Product;
import id.salt.restservice.Models.Responses.ApiResponse;
// import id.salt.restservice.Repositories.ProductRepository;
import id.salt.restservice.Services.ProductService;

@RestController
public class ProductController {

    @Autowired
    private ProductService service;

    @GetMapping("/api/product")
    public ResponseEntity<?> GetProduct(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit)
    {
        List<Product> data = service.getList(page, limit);
        // Object data = null;
        return ResponseEntity.ok().body(new ApiResponse(data));
    }

    @GetMapping("/api/product/{id}")
    public ResponseEntity<?> GetIndividualProduct(@PathVariable long id)
    {
        Product d = service.get(id);
        return ResponseEntity.ok().body(d);
    }

    @PostMapping("/api/product")
    public ResponseEntity<?> InsertData(@RequestBody Product product)
    {
        long id = service.create(product);

        // We must put Public property so the object can be deserialize by jackson fastxml
        // If not it will throw error... zzzz because it return empty
        // @see https://stackoverflow.com/a/61189583/4906348
        return ResponseEntity.ok().body(new ApiResponse("Success insert product",new Object() { public long newId = id;  }));
    }

    @RequestMapping(path = "/api/product/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> DeleteData(@PathVariable long id)
    {
        service.delete(id);
        return ResponseEntity.ok().body(new ApiResponse("Delete Success"));
    }
    
    @RequestMapping(path = "/api/product/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<?> UpdateData(@PathVariable long id, @RequestBody Product product)
    {
        product.setId(id);
        service.update(product);
        return ResponseEntity.ok().body(new ApiResponse("Update Success"));
    }
}
