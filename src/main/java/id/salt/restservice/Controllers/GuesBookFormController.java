package id.salt.restservice.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import id.salt.restservice.Models.Requests.GuestBookRequest;
import jakarta.validation.Valid;

import java.util.*;

@Controller
public class GuesBookFormController {
    // I won't use any DB Backend, I just want to try the Form

    private List<GuestBookRequest> data = new ArrayList<>();

    @GetMapping("/guest-book")
    public String showGuestForm(GuestBookRequest request, Model model)
    {
        model.addAttribute("request", request);
        return "guest-book";
    }

    @GetMapping("/guest-book/list")
    public String showGuestBookList(Model model)
    {
        model.addAttribute("data", this.data);
        return "guest-book-list";
    }

    @PostMapping("/guest-book")
    public String processForm(@Valid GuestBookRequest request, BindingResult bindingResult)
    {
        if (bindingResult.hasErrors()) 
        {
            return "redirect:/guest-book";
        }

        data.add(request);

        return "redirect:/guest-book/list";
    }
}
