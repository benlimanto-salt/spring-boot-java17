package id.salt.restservice.Controllers;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.salt.restservice.Models.Greeting;

@RestController
public class GrettingController {
    private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

    @GetMapping("/greeting")
    public Greeting greetings(@RequestParam(value = "name", defaultValue = "World") String name)
    {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }
    
    @GetMapping("/data")
    public ResponseEntity<?> getData()
    {
        HashMap<String, Object> body = new HashMap<String, Object>();

        body.put("newId", 1);
        return ResponseEntity.ok().body(body);
    }
}
