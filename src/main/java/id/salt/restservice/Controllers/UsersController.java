package id.salt.restservice.Controllers;

import java.util.HashMap;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.salt.restservice.Models.Requests.UserLoginRequest;
import id.salt.restservice.Models.Requests.UserNewRequest;
import id.salt.restservice.Models.Responses.ApiResponse;
import id.salt.restservice.Services.JwtService;
import id.salt.restservice.Services.UserService;
import jakarta.validation.Valid;

@RestController
@RequestMapping(path = "/api/users")
public class UsersController {
    @Autowired
    private UserService service;

    @Autowired
    private PasswordEncoder hasher;

    @Autowired
    private AuthenticationManager authenticationManager; 

    @Autowired
    private JwtService jwtService;

    @GetMapping("/trylike")
    public ResponseEntity<?> GetTryLike(@RequestParam(required = false) String name)
    {
        var data = service.tryLike(name, null);

        return ResponseEntity.ok().body(new ApiResponse("what for me", data));
    }

    @GetMapping("")
    public ResponseEntity<?> GetList(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit)
    {
        var data = service.getList(page, limit);

        return ResponseEntity.ok().body(new ApiResponse(data));
    }
    
    @GetMapping("{idOrUsername}")
    public ResponseEntity<?> GetIndividual(@PathVariable() String idOrUsername)
    {
        Object data;

        // The only way to handle one parameter either string or number.. 
        try {
            Integer id = Integer.parseInt(idOrUsername);
            data = service.get(id);            
        } catch (Exception e) {
            data = service.get(idOrUsername);
        }

        return ResponseEntity.ok().body(new ApiResponse(data));
    }

    @PostMapping()
    public ResponseEntity<?> InsertUser(@RequestBody UserNewRequest request)
    {
    
        request.password = hasher.encode(request.password);
        Integer id = service.insert(request.ToUser());

        HashMap<String, Object> response = new HashMap<>();
        response.put("newId", id);

        return ResponseEntity.ok().body(new ApiResponse("Create user success", response));
    }

    /**
     * This is simple auth, but doesn't return JWT. Still researching
     * @param request
     * @param bindingResult
     * @return
     */
    @PostMapping("/login-manual")
    public ResponseEntity<?> LoginAction(@RequestBody @Valid UserLoginRequest request, BindingResult bindingResult)
    {
        if (bindingResult.hasErrors())
        {
            return new ResponseEntity<>(new ApiResponse(400, "Error validation", bindingResult.getAllErrors()), HttpStatus.BAD_REQUEST);
        }

        var u = service.get(request.username);
        
        if (u == null) 
        {
            return new ResponseEntity<>(new ApiResponse(400, "User or Pass wrong"), HttpStatus.BAD_REQUEST);
        }

        boolean result = hasher.matches(request.password, u.getPassword());

        if (!result) 
        {
            return new ResponseEntity<>(new ApiResponse(400, "User or Pass wrong"), HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok().body(new ApiResponse("Success Login"));
    }

    /**
     * This quite near the code of Geeks 2 Geeks
     * @see https://www.geeksforgeeks.org/spring-boot-3-0-jwt-authentication-with-spring-security-using-mysql-database
     * 
     * for Other JWT.io, we can see 
     * @see https://www.freecodecamp.org/news/how-to-setup-jwt-authorization-and-authentication-in-spring/
     * @param request
     * @param bindingResult
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<?> LoginAuth(@RequestBody @Valid UserLoginRequest request, BindingResult bindingResult)
    {
        if (bindingResult.hasErrors())
        {
            return new ResponseEntity<>(new ApiResponse(400, "Error validation", bindingResult.getAllErrors()), HttpStatus.BAD_REQUEST);
        }

        var u = service.get(request.username);
        
        if (u == null) 
        {
            return new ResponseEntity<>(new ApiResponse(400, "User or Pass wrong"), HttpStatus.BAD_REQUEST);
        }

        var authReq = new UsernamePasswordAuthenticationToken(request.username, request.password);
        Authentication auth = authenticationManager.authenticate(authReq);

        if (!auth.isAuthenticated()) 
        {
            return new ResponseEntity<>(new ApiResponse(400, "User or Pass wrong"), HttpStatus.BAD_REQUEST);
        }

        Map<String, Object> response = new HashMap<String, Object>();
        String token = jwtService.generateToken(request.username);

        response.put("token", token);
        return ResponseEntity.ok().body(new ApiResponse("Success Login", response));
    }
}
