package id.salt.restservice.Controllers;

import java.io.InputStream;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import id.salt.restservice.Models.Responses.ApiResponse;
import id.salt.restservice.Services.StorageService;

@RestController
public class FileUploadController {
    private StorageService storageService;

    public FileUploadController(StorageService srv)
    {
        storageService = srv;
    }

    @PostMapping("/upload")
    public ResponseEntity<?> UploadAction(@RequestParam("file") MultipartFile requestFile)
    {
        try {
            storageService.store(requestFile);
        }
        catch (Exception e) {
            return ResponseEntity.internalServerError().body(new ApiResponse(500, "error upload", e.getMessage()));
        }
        return ResponseEntity.ok().body(new ApiResponse("succes upload file"));
    }

    // This is for stream from Body
    @PostMapping("/upload-stream")
    public ResponseEntity<?> UploadAction(InputStream requestFile)
    {
        try {
            storageService.store(requestFile);
        }
        catch (Exception e) {
            return ResponseEntity.internalServerError().body(new ApiResponse(500, "error upload", e.getMessage()));
        }
        return ResponseEntity.ok().body(new ApiResponse("succes upload file"));
    }
}
