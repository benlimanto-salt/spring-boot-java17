package id.salt.restservice.Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import id.salt.restservice.Middleware.JwtAuthFilter;
import id.salt.restservice.Services.UserService;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecSecurityConfig {

    @Autowired
    private JwtAuthFilter jwtAuthFilter;

    @Bean
    public SecurityFilterChain allowAnonymousFilterChain(HttpSecurity http) throws Exception {
        http
        .csrf((c) -> c.disable())
		.authorizeHttpRequests((authorize) -> authorize
            // Allow Login Anon
            .requestMatchers("/api/users/login").permitAll()
            .requestMatchers("/guest-book").permitAll()
            .requestMatchers("/guest-book/**").permitAll()
            .requestMatchers("/greeting").permitAll()
            .requestMatchers("/api/**").hasAnyRole("USER", "ADMIN") // for multiple roles
            // .requestMatchers("/api/**").authenticated()
            // Disable all the rest security
			// .anyRequest().anonymous()
		)
        .sessionManagement(s -> s.sessionCreationPolicy(SessionCreationPolicy.STATELESS)) 
        .authenticationProvider(authenticationProvider())
        .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

	    return http.build();
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(15);
    }

    @Bean
    public UserDetailsService userDetailsService()
    {
        return new UserService();
    }

    // See https://www.geeksforgeeks.org/spring-boot-3-0-jwt-authentication-with-spring-security-using-mysql-database
    @Bean
    public AuthenticationProvider authenticationProvider() { 
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider(); 
        authenticationProvider.setUserDetailsService(userDetailsService()); 
        authenticationProvider.setPasswordEncoder(passwordEncoder()); 
        return authenticationProvider; 
    } 

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception { 
        return config.getAuthenticationManager(); 
    } 
}
