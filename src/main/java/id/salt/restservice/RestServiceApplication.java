package id.salt.restservice;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
// import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import id.salt.restservice.Config.StorageProperties;
import id.salt.restservice.Services.StorageService;

// @EntityScan("id.salt.restservice.Repositories")
// @EnableJpaRepositories("id.salt.restservice.Repositories")
@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class) // For the config... ugh..
// @EnableJpaAuditing
public class RestServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner init(StorageService storageService) {
		return (args) -> {
			storageService.deleteAll();
			storageService.init();
		};
	}

}
